# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 09:08:12 2021

@author: Administrador
"""

import random
from itertools import combinations

def generarMazo():
    Mazo = [1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10]
    random.shuffle(Mazo)
    return Mazo


def repartir(mazo, jug1, jug2, jug3, jug4):
    if len(mazo)>0:
        i=1
        while i <=3:
            jug1.append(mazo.pop(0))
            jug2.append(mazo.pop(0))
            jug3.append(mazo.pop(0))
            jug4.append(mazo.pop(0))
            i=i+1
    else:
        print("Última mano")
    return mazo


def iniciarMesa(mazo,mesa):
    i=1
    while i <=4:
        mesa.append(mazo.pop(0))
        i=i+1
    return mesa


def subListas(lista):
    resultado = []
    for k in range (1, len(lista)+1):
        for listita in combinations(lista,k):
            resultado.append(list(listita))
    return resultado
# sublistas da todas las posibles combinaciones dentro de una lista


def juegosPosibles(jug, mesa):
    i=0
    j=0
    lista=[]
    while j<len(mesa):
        lista.append(mesa[j])
        j=j+1
    jugadas=[]
    jugadas15=[]
    while i<len(jug):  
        resultado = subListas(lista) 
        ii=0
        while ii<len(resultado): 
            resultado[ii].append(jug[i])
            ii=ii+1
        jugadas.append(resultado)
        i=i+1
    k=0
    while k<len(jugadas):
        l=0
        while l<len(jugadas[k]):
            sumita=0
            m=0
            while m<len(jugadas[k][l]):
                sumita=sumita+jugadas[k][l][m]
                m=m+1
            if sumita ==15:
                jugadas15.append(jugadas[k].pop(l))        
            l=l+1
        k=k+1
    return jugadas15


def elegirMejor(juegos):
    i=0
    mejor=juegos[i]
    while i<len(juegos):
        if len(juegos[i])>len(mejor):
            mejor=juegos[i]
        i=i+1
    return mejor


def jugar(mesa,jug,basa):
    jugadas=juegosPosibles(jug, mesa)
    if len(jugadas)>0:
        mejor=elegirMejor(jugadas)
        k=0
        while k<len(mesa):
            j=0
            saco=False
            while j<(len(mejor)-1) and saco==False:
                if mesa[k]==mejor[j]:
                    basa.append(mesa.pop(k))
                    saco=True
                else:
                    j=j+1
            k=k+1
        i=0
        mano=len(jug)        
        while i<len(jug):
            if len(jug)==mano:
                if jug[i]==mejor[len(mejor)-1]:
                    basa.append(jug.pop(i))
            i=i+1
    else:
        mesa.append(jug.pop(0))
    return mesa


def jugarRonda(mesa, jug1, basa1, jug2, basa2, jug3, basa3, jug4, basa4):
    jugar(mesa, jug1, basa1)
    jugar(mesa, jug2, basa2)
    jugar(mesa, jug3, basa3)
    jugar(mesa, jug4, basa4)
    jugar(mesa, jug1, basa1)
    jugar(mesa, jug2, basa2)
    jugar(mesa, jug3, basa3)
    jugar(mesa, jug4, basa4)
    jugar(mesa, jug1, basa1)
    jugar(mesa, jug2, basa2)
    jugar(mesa, jug3, basa3)
    jugar(mesa, jug4, basa4)
    return mesa


def sumaPuntos(basa1, basa2, basa3,basa4):
    if len(basa1)>len(basa2) and len(basa1)>len(basa3) and len(basa1)>len(basa4):
        print(basa1, basa2, basa3, basa4, "ganó el jugador 1")
    elif len(basa2)>len(basa1) and len(basa2)>len(basa3) and len(basa2)>len(basa4):
        print(basa1, basa2, basa3, basa4, "ganó el jugador 2")
    elif len(basa3)>len(basa1) and len(basa3)>len(basa2) and len(basa3)>len(basa4):
        print(basa1, basa2, basa3, basa4, "ganó el jugador 3")
    elif len(basa4)>len(basa1) and len(basa4)>len(basa3) and len(basa4)>len(basa2):
        print(basa1, basa2, basa3, basa4, "ganó el jugador 4")
    else:
        print(basa1, basa2, basa3, basa4, "no ganó nadie")
    return 

def JugarEscoba():
    J1 = []
    J2 = []
    J3 = []
    J4 = []
    basaJ1=[]
    basaJ2=[]
    basaJ3=[]
    basaJ4=[]
    Mesa=[]
    Mazo=generarMazo()
    repartir(Mazo,J1,J2,J3,J4)
    iniciarMesa(Mazo, Mesa)
    jugarRonda(Mesa, J1, basaJ1, J2, basaJ2, J3, basaJ3, J4, basaJ4)
    repartir(Mazo,J1,J2,J3,J4)
    jugarRonda(Mesa, J1, basaJ1, J2, basaJ2, J3, basaJ3, J4, basaJ4)
    repartir(Mazo,J1,J2,J3,J4)
    jugarRonda(Mesa, J1, basaJ1, J2, basaJ2, J3, basaJ3, J4, basaJ4)
    sumaPuntos(basaJ1, basaJ2, basaJ3, basaJ4)
    return

JugarEscoba()
